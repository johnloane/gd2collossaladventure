import java.io.*;
import java.util.*;

public class Locations implements Map<Integer, Location>
{
    private static Map<Integer, Location> locations = new LinkedHashMap<Integer, Location>();

    public static void main (String[] args)
    {
        writeLocationAndExitDataToTextFile();
        writeLocationAndExitDataToBinaryFile();
        writeOSMagic();


        //Write out to binary file


//        FileWriter locationsFile = null;
//        try {
//            locationsFile = new FileWriter("locations.txt");
//            for (Location location : locations.values()) {
//                locationsFile.write(location.getLocationID() + "," + location.getDescription() + "\n");
//            }
//            locationsFile.close();
//        }
//        finally
//        {
//            System.out.println("In finally block");
//            if(locationsFile != null) {
//                System.out.println("Attempting to close locationsFile");
//                locationsFile.close();
//            }
//        }
    }

    public static void writeOSMagic()
    {
        try(ObjectOutputStream locationsFileOS = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("locationsOS.bin"))))
        {
            for(Location location : locations.values())
            {
                locationsFileOS.writeObject(location);
            }
        }catch(IOException e)
        {
            System.out.println("IO Exception");
        }
    }

    public static void writeLocationAndExitDataToTextFile()
    {
        try(FileWriter locationsFile = new FileWriter("locations.txt");
            FileWriter directionsFile = new FileWriter("directions.txt"))
        {
            for(Location location : locations.values())
            {
                locationsFile.write(location.getLocationID() + "," + location.getDescription() + "\n");
                for(String direction : location.getExits().keySet())
                {
                    if(!direction.equalsIgnoreCase("Q")) {

                        directionsFile.write(location.getLocationID() + "," + direction + "," + location.getExits().get(direction) + "\n");
                    }
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void writeLocationAndExitDataToBinaryFile()
    {
        try(DataOutputStream locationAndExitFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(("locations.bin")))))
        {
            for(Location location : locations.values())
            {
                locationAndExitFile.writeInt(location.getLocationID());
                locationAndExitFile.writeUTF(location.getDescription());
                System.out.println("Writing location " + location.getLocationID() + ": " + location.getDescription());

                /* The Q exit exits for every location and is added in the constructor so we don't need to write it out. This is where the -1 comes from*/

                System.out.println("Writing " + (location.getExits().size()-1) + " exits");
                locationAndExitFile.writeInt(location.getExits().size()-1);
                for(String direction : location.getExits().keySet())
                {
                    if(!direction.equalsIgnoreCase("Q"))
                    {
                        System.out.println("\t\t" + direction + ", " + location.getExits().get(direction));
                        locationAndExitFile.writeUTF(direction);
                        locationAndExitFile.writeInt(location.getExits().get(direction));
                    }
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    static
    {
        //readLocationsAndExitsFromTextFile();
        readLocationsAndExitsFromBinaryFile();
    }

    private static void readLocationsAndExitsFromBinaryFile()
    {
        try(DataInputStream locationsFile = new DataInputStream(new BufferedInputStream(new FileInputStream("locations.bin"))))
        {
            boolean eof = false;
            while (eof == false) {
                try {
                    Map<String, Integer> exits = new LinkedHashMap();
                    int locID = locationsFile.readInt();
                    String description = locationsFile.readUTF();
                    System.out.println("Read location" + locID + " : " + description);
                    int numExits = locationsFile.readInt();
                    System.out.println("Found " + numExits + " exits");
                    for (int i = 0; i < numExits; i++) {
                        String direction = locationsFile.readUTF();
                        int destination = locationsFile.readInt();
                        exits.put(direction, destination);
                        System.out.println("\t\t" + direction + ", " + destination);
                    }
                    locations.put(locID, new Location(locID, description, exits));
                } catch (EOFException e) {
                    eof = true;
                }
            }
        }catch(IOException e)
        {
            System.out.println("IO Exception");
        }
    }

    private static void readLocationsAndExitsFromTextFile()
    {
        try(Scanner scanner = new Scanner(new FileReader("locations_big.txt")))
        {
            scanner.useDelimiter(",");
            while (scanner.hasNextLine()) {
                int loc = scanner.nextInt();
                scanner.skip(scanner.delimiter());
                String description = scanner.nextLine();
                System.out.println("Imported location: " + loc + ": " + description);
                Map<String, Integer> tempExit = new HashMap<>();
                locations.put(loc, new Location(loc, description, tempExit));
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }


        //Now read the exits. This is for homework.
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("directions_big.txt"))))
        {
            while(scanner.hasNextLine())
            {
//                int loc = scanner.nextInt();
//                scanner.skip(scanner.delimiter());
//                String direction = scanner.next();
//                scanner.skip(scanner.delimiter());
//                String dest = scanner.nextLine();
//                int destination = Integer.parseInt(dest);
                String input = scanner.nextLine();
                String[] data = input.split(",");
                int loc = Integer.parseInt(data[0]);
                String direction = data[1];
                int destination = Integer.parseInt(data[2]);

                System.out.println(loc + ": " + direction + ": " + destination);
                Location location = locations.get(loc);
                location.addExit(direction, destination);
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public int size()
    {
        return locations.size();
    }

    @Override
    public boolean isEmpty()
    {
        return locations.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return locations.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return locations.containsValue(value);
    }

    @Override
    public Location get(Object key)
    {
        return locations.get(key);
    }

    @Override
    public Location put(Integer key, Location value)
    {
        return locations.put(key, value);
    }

    @Override
    public Location remove(Object key)
    {
        return locations.remove(key);
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Location> m)
    {

    }

    @Override
    public void clear()
    {
        locations.clear();
    }

    @Override
    public Set<Integer> keySet()
    {
        return locations.keySet();
    }

    @Override
    public Collection<Location> values()
    {
        return locations.values();
    }

    @Override
    public Set<Entry<Integer, Location>> entrySet()
    {
        return locations.entrySet();
    }
}

/* Implements writeInt for binary files - uses bitshifting*/
public class ShiftInt
{
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args)
    {
//        System.out.println(ANSI_CYAN + "Hello in colour");
//        System.out.println("Something else");
        int x = 922342959;
        writeInt(x);
    }

    private static void writeInt(int v)
    {
        display(v >>> 24);
        display(v >>> 16);
        display(v >>> 8);
        display(v >>> 0);
    }

    private static void display(int x)
    {
        String all = String.format("%32s", Integer.toBinaryString(x)).replace(" ", "0");
        String colourBinary = all.substring(0,24) + ANSI_PURPLE + all.substring(24) + ANSI_RESET;
        int y = x & 0xFF;
        String output = String.format("%10d and 0xFF is %8s \t %10d is ", y, Integer.toBinaryString(y), x) + colourBinary;
        System.out.println(output);
    }
}
